<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostsController extends Controller
{
    protected $posts;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Post $post)
    {
        $this->posts = $post;
    }

    public function index()
    {
        //v1.2.0
        $posts = $this->posts->paginate(5);
        $resource = PostResource::collection($posts)->response()->getData(true);
        return $this->SentResourceResponse($resource, 'success', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $dataCreate = $request->all();
        $post = $this->posts->create($dataCreate);
        $response = new PostResource($post);
        return $this->SentResourceResponse($response, 'success', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->posts->findOrFail($id);
        $resource = new PostResource($post);
        return $this->SentResourceResponse($resource, 'success', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $dataUpdate = $request->all();
        $post = $this->posts->findOrFail($id);
        $post->update($dataUpdate);
        $resource = new PostResource($post);
        return $this->SentResourceResponse($resource, 'success', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->posts->findOrFail($id);
        $post->delete();
        $resource = new PostResource($post);
        return $this->SentResourceResponse($resource, 'success', Response::HTTP_OK);
    }
}
